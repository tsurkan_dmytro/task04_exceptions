package com.task.exeption.exeptions;

public class IncorrectSrudentNameException extends Exception {
    public IncorrectSrudentNameException(String errorMessage) {
        super(errorMessage);
    }
}
