package com.task.exeption.exeptions;

public class StudentIdException extends NumberFormatException {
    public StudentIdException(String errorMessage) {
        super(errorMessage);
    }
}
