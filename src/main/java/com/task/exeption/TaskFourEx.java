package com.task.exeption;

import com.task.exeption.controller.StudentController;
import com.task.exeption.exeptions.IncorrectSrudentNameException;
import com.task.exeption.model.Student;
import com.task.exeption.view.StudentView;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TaskFourEx {



    public static void main(String[] args) {
        List<Student> studentDb = new LinkedList<>();
        //fetch student record based on his roll no from the database
        StudentView view = new StudentView();

        //Create a view : to write student details on console
        StudentController controller = null;

        for ( int i = 1; i <= 25; i++){
            Student model = retriveStudentFromDatabase(i);
            controller = new StudentController(model, view);
            controller.updateView();

            //update model data
            try {
                controller.setStudentName("John " + i );
                controller.writeToFile();
            } catch (IncorrectSrudentNameException e) {
                System.out.println("Wrong student name");
            }
            controller.updateView();
        }
    }

    private static Student retriveStudentFromDatabase(int n){
            Student student = new Student();
            student.setId(n);
            student.setName("Robert " + n);
            student.setRollNo("10" + n);
        return student;
    }
}
