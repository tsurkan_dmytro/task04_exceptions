package com.task.exeption.model;

import com.task.exeption.exeptions.StudentIdException;

public class Student {
    private int id;
    private String rollNo;
    private String name;

    public String getRollNo() {
        return rollNo;
    }
    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id > 0 ){
            this.id = id;
        }else {
            throw new StudentIdException("Wrong student id");
        }
    }
}
