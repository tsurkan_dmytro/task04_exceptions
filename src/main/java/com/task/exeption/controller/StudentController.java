package com.task.exeption.controller;

import com.task.exeption.exeptions.IncorrectSrudentNameException;
import com.task.exeption.model.Student;
import com.task.exeption.view.StudentView;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StudentController {
    private Student model;
    private StudentView view;

    public StudentController(Student model, StudentView view){
        this.model = model;
        this.view = view;
    }

    public void setStudentName(String name) throws IncorrectSrudentNameException {
        model.setName(name);
    }

    public String getStudentName(){
        return model.getName();
    }

    public int getStudentId(){
        return model.getId();
    }

    public void setStudentRollNo(String rollNo){
        model.setRollNo(rollNo);
    }

    public String getStudentRollNo(){
        return model.getRollNo();
    }

    public void updateView(){
        view.printStudentDetails(model.getName(), model.getRollNo());
    }

    public void writeToFile(){
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("studentsData.txt",true))){
            if (getStudentName() != null) {
                writer.write("\n");
                writer.write(getStudentId()+"\n");
                writer.write(getStudentRollNo()+"\n");
                writer.write(getStudentName()+"\n");
                writer.write("--------"+"\n");
            }
        }
        catch(IOException e){
            System.out.println("Cant write to file");
        }
    }

    public int getRandomNum(){
        int rdm = 1 + (int)(Math.random() * ((99 - 1) + 1));
        return rdm;
    }
}